# Hi 👋, I'm Creerio

## About me

A Linux user who enjoys coding things.

## Languages & Projects

### <a href="https://www.java.com" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=java" alt="java" width="40" height="40"/> Java </a>

- [NoScroll](https://gitlab.com/crepes-o-mods/noscroll) : A [Minecraft](https://www.minecraft.net/) Java Edition fabric mod used to disable hotbar scrolling using a hotkey. Partially maintained, archived.

- [Elemental enchantments](https://gitlab.com/crepes-o-mods/elemental-enchantments) : A [Minecraft](https://www.minecraft.net/) Java Edition fabric mod adding simple elemental enchantments.

- [Dig Under You](https://gitlab.com/Creerio/dig-under-you) : A [Minecraft](https://www.minecraft.net/) Java Edition [Spigot](https://www.spigotmc.org/) plugin for a simple game. Archived.

- [Minekraft form](https://gitlab.com/Creerio/minekraft-form) : An android 'parody' survey uni project. Archived.

### <a href="https://kernel.org/" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=linux" alt="linux" width="40" height="40"/> Linux 'projects' </a>

- [flakes](https://gitlab.com/Creerio/flakes) : A repository for my nixos config. May be of use if you are starting to use nixos as it is a quite simple config.

- [scripts](https://gitlab.com/Creerio/scripts) : A repository containing some useful bash scripts, mainly for personal use. (Currently contains a snap removal script, a mesa builder w/ hardware encoding/decoding script for Manjaro and an installer for pipewire made for arch based distros)

### <a href="https://nodejs.org" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=nodejs" alt="nodejs" width="40" height="40"/> NodeJS</a>

- [Crepes O' Fun](https://gitlab.com/crepes-o-disbots/crepes-o-fun) : A discord bot for 'fun' elements. Based upon my [Crepes O' Core](https://gitlab.com/crepes-o-disbots/crepes-o-core) bot template. Uses a database (any supported by [Sequelize](https://sequelize.org/)) to store specific information.

- [Zoo Phylium](https://gitlab.com/Creerio/zoo-phylium) : A uni project made with React & Express. Should you want to look at this, I greatly recommend to either know french or have a translator. Also, you might need some humor. Archived.

- [Packwiz to Markdown](https://gitlab.com/Creerio/packwiz-to-md) : A program to read all [packwiz](https://github.com/packwiz/packwiz) mods/resource packs from a folder and extract them as a markdown.

### <a href="https://www.w3schools.com/cpp/" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=cpp" alt="cplusplus" width="40" height="40"/> C++ </a>

- [TurboTurtle](https://gitlab.com/turboturtle/turboturtle) : A uni project for the [EESI](https://www.eesi.eu/) art school. This software allows the user to draw animations and/or create stop motion animations with live drawing support using [QT](https://www.qt.io/), [OpenCV](https://opencv.org/) and the [NDI](https://ndi.video/)™ protocol.

- [LabyResolve](https://gitlab.com/Creerio/labyresolve) : A uni project made to resolve mazes. Should you want to look at this, I greatly recommend to either know french or have a translator. Archived.

### <a href="https://symfony.com" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=symfony" alt="symfony" width="40" height="40"/> Symfony</a>

- [Symfony web project - Market](https://gitlab.com/Creerio/symfony-web-project-market) : A small market website uni project. Archived.



### Other languages/Tools:
<p align="left">
    <a href="https://www.gnu.org/software/bash/" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=bash" alt="bash" width="40" height="40"/></a>
    <a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=docker" alt="docker" width="40" height="40"/> </a>  
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=javascript" alt="javascript" width="40" height="40"/> </a> 
    <a href="https://www.php.net" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=php" alt="php" width="40" height="40"/> </a> 
    <a href="https://cloudflare.com/" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=cloudflare" alt="cloudflare" width="40" height="40"/> </a> 
    <a href="https://nginx.org" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=nginx" alt="nginx" width="40" height="40"/> </a> 
    <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=html" alt="html5" width="40" height="40"/> </a> 
    <a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=css" alt="css3" width="40" height="40"/> </a> 
    <a href="https://www.qt.io/" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=qt" alt="qt" width="40" height="40"/> </a> <a href="https://www.cprogramming.com/" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=c" alt="c" width="40" height="40"/> </a> 
    <a href="https://www.postgresql.org/" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=postgres" alt="mariadb" width="40" height="40"/> </a> 
    <a href="https://www.python.org" target="_blank" rel="noreferrer"> <img src="https://skillicons.dev/icons?i=python" alt="python" width="40" height="40"/> </a> 
</p>
